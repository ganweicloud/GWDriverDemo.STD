﻿namespace GWDriverDemo.STD;

/// <summary>
/// 连接参数类
/// 不同驱动连接参数不一样，可以根据实际情况去定义与设备或者系统的连接属性。
/// </summary>
public class ConnectionConfig
{
    /// <summary>
    /// 连接地址，可以是任意的连接地址，如TCP，Http，OPC，Modbus等。
    /// </summary>
    public string ServerUrl { get; set; }

    /// <summary>
    /// 账号密码
    /// </summary>
    public string UserName { get; set; }

    /// <summary>
    /// 密码
    /// </summary>
    public string Password { get; set; }

    /// <summary>
    /// 证书
    /// </summary>
    public string CertificatePath { get; set; }

    /// <summary>
    /// 证书密码
    /// </summary>

    public string CertificatePwd { get; set; }
}
