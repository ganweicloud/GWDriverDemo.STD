﻿namespace GWDriverDemo.STD;

public class ClientSession
{
    /// <summary>
    /// 此处更换具体的连接对象，如OPC,MQTT,Http的连接对象。
    /// </summary>
    public object Session { get; private set; }

    /// <summary>
    /// 连接配置信息，主要用于重连
    /// </summary>
    public ConnectionConfig ConnectionConfig { get; private set; }

    /// <summary>
    /// 当前连接下，设备及对应的属性内容，此次的object可以根据设备遥测遥信来设计模型
    /// </summary>
    public Dictionary<int, object> CurrentValues { get; private set; }

    /// <summary>
    /// 当前连接下，设备的事件信息。
    /// </summary>
    public Dictionary<int, List<EquipEventModel>> CurrentEvents { get; private set; }

    /// <summary>
    /// 当前设备连接参数
    /// </summary>
    public bool Status = false;

    public ClientSession(ConnectionConfig connectionConfig, object session)
    {
        ConnectionConfig = connectionConfig;
        Session = session ?? throw new ArgumentNullException(nameof(session));
        Status = true;
        CurrentValues = new Dictionary<int, object>();
        CurrentEvents = new Dictionary<int, List<EquipEventModel>>();
    }

    public async Task ReSetClientSession(object resession)
    {
        Session = resession;
        await Task.CompletedTask;
    }

    public void AddCurrentValue(int equipNo, object value)
    {
        CurrentValues[equipNo] = value;
    }

    public void AddCurrentEvent(int equipNo, List<EquipEventModel> values)
    {
        if (CurrentEvents.ContainsKey(equipNo))
        {
            CurrentEvents[equipNo].AddRange(values);
        }
        else
        {
            CurrentEvents[equipNo] = values;
        }
    }

    public void SetOnline()
    {
        Status = true;
    }
    public void SetOffline()
    {
        Status = false;
    }

    public void DisposeSession()
    {
        try
        {
            CurrentValues.Clear();

            // 关闭会话
            if (Session != null)
            {
                //Session?.Close();
                //Session?.Dispose();
            }
            // 设置状态为离线
            SetOffline();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Failed to dispose session for {ConnectionConfig.ServerUrl}: {ex.Message}");
        }
    }
}
