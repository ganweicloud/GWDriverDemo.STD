﻿using System.Security.Cryptography.X509Certificates;

namespace GWDriverDemo.STD;

public class ConnClientManager
{
    private static readonly Lazy<ConnClientManager> instance = new Lazy<ConnClientManager>(() => new ConnClientManager());

    /// <summary>
    /// OPC连接对象池
    /// </summary>
    private Dictionary<string, ClientSession> clientSessions = new Dictionary<string, ClientSession>();

    /// <summary>
    /// 全局锁，用于添加连接对象时进行
    /// </summary>
    private readonly object lockObject = new object();

    private ConnClientManager() { }

    public static ConnClientManager Instance
    {
        get { return instance.Value; }
    }

    /// <summary>
    /// 添加连接对接
    /// 需要确保当前方法可以重复执行，应避免多次创建连接对象。
    /// </summary>
    /// <param name="connectionConfig"></param>
    public void CreateClientSession(ConnectionConfig connectionConfig)
    {
        lock (lockObject)
        {
            if (!clientSessions.TryGetValue(connectionConfig.ServerUrl, out ClientSession clientSession))
            {
                try
                {
                   //TODO 创建连接
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Failed to connect to server {connectionConfig.ServerUrl}: {ex.Message}");
                }
            }
            else
            {
                if (clientSession.Session == null)
                {
                    clientSessions.Remove(connectionConfig.ServerUrl);
                    return;
                }
            }
        }
    }

    /// <summary>
    /// 添加订阅，适用于OPC，MQTT等订阅模式的协议
    /// </summary>
    /// <param name="serverUrl">连接地址</param>
    /// <param name="equipNo">设备号</param>
    public void AddSubscription(string serverUrl, string equipNo)
    {
        try
        {
            if (clientSessions.TryGetValue(serverUrl, out ClientSession clientSession))
            {
                if (clientSession.Session == null)
                {
                    clientSessions.Remove(serverUrl);
                    return;
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"AddSubscription ex：{ex}");
        }
    }

    /// <summary>
    /// 向设备下发控制命令
    /// 注意：该方法的具体实现根据协议的来处理，开发人员可以修改入参等内容。
    /// </summary>
    /// <param name="serverUrl">连接地址</param>
    /// <param name="controlMethod">控制方法</param>
    /// <param name="value">传入值</param>
    /// <returns></returns>
    public object WriteValueAsync(string serverUrl, string controlMethod, object value)
    {
        try
        {
            if (clientSessions.TryGetValue(serverUrl, out ClientSession clientSession))
            {
                //TODO 通过clientSession连接对象下发数据
                return new { Code = 200,Msg = "执行成功" };
            }
            else
            {
                Console.WriteLine($"Session for server {serverUrl} not found.");
                return new { Code = 400, Msg = "连接对象不存在" };
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception occurred while writing value: {ex.Message}");
            return new { Code = 500, Msg = "执行异常：" + ex.Message };
        }
    }


    /// <summary>
    /// 获取当前连接实例的状态
    /// </summary>
    /// <param name="serverUrl"></param>
    /// <returns></returns>
    public bool GetClientSessionStatus(string serverUrl)
    {
        try
        {
            if (clientSessions.TryGetValue(serverUrl, out var clientSession))
            {
                return clientSession.Status;
            }
            else
            {
                return false;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception occurred while writing value: {ex.Message}");
            return false;
        }
    }

    /// <summary>
    /// 获取当前连接地址，当前设备采集的实时数据
    /// </summary>
    /// <param name="serverUrl"></param>
    /// <param name="nodeId"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public Dictionary<string, object> GetCurrentValues(string serverUrl, int equipNo)
    {
        try
        {
            //TODO 获取某个设备号的所有实时数据，此次可以是直接调用接口获取，如Http的那种。
            //对于MQTT、TCP、OPC这种，主动接收传输的数据时，按照设备号与属性的关联关系，缓存一组最新的实时数据。
            return new Dictionary<string, object>();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception occurred while getting values: {ex.Message}");
            return new Dictionary<string, object>();
        }
    }

    /// <summary>
    /// 获取当前连接地址，当前设备产生的事件记录
    /// </summary>
    /// <param name="serverUrl"></param>
    /// <param name="nodeId"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public List<EquipEventModel> GetCurrentEvents(string serverUrl, int equipNo)
    {
        try
        {
            if (clientSessions.TryGetValue(serverUrl, out var clientSession))
            {
                //事件记录都是递增方式添加事件，相对于属性值而言，正常业务不希望事件记录丢失，开发者可以对事件记录数据做一份落库更保险。
                //本次获取后将数据移除，防止内存单个设备事件记录一直增加。
                if (clientSession.CurrentEvents.Remove(equipNo, out var equipEventModels))
                    return equipEventModels;
            }
            else
            {
                return null;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Exception occurred while writing value: {ex.Message}");
            return null;
        }
        return null;
    }


    /// <summary>
    /// 加载证书
    /// </summary>
    /// <param name="certificatePath"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentException"></exception>
    private X509Certificate2 LoadCertificate(string certificatePath, string password = null)
    {
        if (string.IsNullOrEmpty(certificatePath))
        {
            throw new ArgumentException("Certificate path is required for certificate authentication.");
        }
        return new X509Certificate2(certificatePath, password);
    }
}